package br.mobile.stefanini;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Fabio on 04/01/2018.
 */

public class ImageSelect extends AppCompatActivity {

    //constantes
    public static final int IMAGE_GALLERY_REQUEST = 20;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA_PERMISSION = 200;
    public static final String EXTRA_STORE_ARRAY_INDEX = "store_array_index";

    //atributos da tela
    public Button btnCamera;
    public Button btnGalery;
    public Button btnCancel;
    public Button btnTake;
    public TextureView tvTexture;
    public ImageView ivImage;

    //index do array de lojas (usado para guardar a imagem)
    public int arrayIndex;

    //atributos referente a camera 2
    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;

    //atributos referente ao salvamento da imagem
    private File file;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    //checa a orientação da imagem
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static{
        ORIENTATIONS.append(Surface.ROTATION_0,90);
        ORIENTATIONS.append(Surface.ROTATION_90,0);
        ORIENTATIONS.append(Surface.ROTATION_180,270);
        ORIENTATIONS.append(Surface.ROTATION_270,180);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_select);

        this.btnCamera = (Button) findViewById(R.id.btnCamera);
        this.btnGalery = (Button) findViewById(R.id.btnGalery);
        this.btnCancel = (Button) findViewById(R.id.btnCancel);
        this.btnTake = (Button) findViewById(R.id.btnTake);
        this.tvTexture = (TextureView) findViewById(R.id.tvTexture);
        this.ivImage = (ImageView) findViewById(R.id.ivImage);

        this.arrayIndex = getIntent().getExtras().getInt(EXTRA_STORE_ARRAY_INDEX);

        //deixa invisiveis pois apenas ficarao visiveis quando
        //o botao para tirar foto for pressionado
        this.btnCancel.setVisibility(View.INVISIBLE);
        this.btnTake.setVisibility(View.INVISIBLE);
        this.tvTexture.setVisibility(View.INVISIBLE);

        checkImage();

        assert tvTexture != null;
        tvTexture.setSurfaceTextureListener(textureListener);

        //listeners
        this.btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                btnCancel.setVisibility(View.VISIBLE);
                btnTake.setVisibility(View.VISIBLE);
                tvTexture.setVisibility(View.VISIBLE);

                btnCamera.setVisibility(View.INVISIBLE);
                btnGalery.setVisibility(View.INVISIBLE);
                ivImage.setVisibility(View.INVISIBLE);
            }
        });

        this.btnGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //abaixo verifica se o usuario já permitiu ler sd
                if (ContextCompat.checkSelfPermission(ImageSelect.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    //caso retorne PERMISSION_GRANTED, ele nao tem permitido, logo
                    //vai ter que solicitar agora
                    ActivityCompat.requestPermissions(ImageSelect.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    //ao final deste metodo, ele vai chamar o callback onRequestPermissionsResult
                    //dizendo se o usuario permitiu ou não
                }else{
                    //se entrar aqui, é pq já tem liberado
                    selectImage();
                }
            }
        });

        this.btnTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                takePicture();
            }
        });

        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                btnCancel.setVisibility(View.INVISIBLE);
                btnTake.setVisibility(View.INVISIBLE);
                tvTexture.setVisibility(View.INVISIBLE);

                btnCamera.setVisibility(View.VISIBLE);
                btnGalery.setVisibility(View.VISIBLE);
                checkImage();
            }
        });
    }

    public void checkImage(){
        if(MainActivity.images[arrayIndex] != null){
            this.ivImage.setImageBitmap(MainActivity.images[arrayIndex]);
            this.ivImage.setVisibility(View.VISIBLE);
        }else{
            this.ivImage.setVisibility(View.INVISIBLE);
        }
    }

    //metodos referente a galeria

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == ImageSelect.IMAGE_GALLERY_REQUEST){
                //entrando aqui significa que nao houve erros e que esse resultado
                //foi após ir para a galeria

                //endereço da imagem no celular
                Uri imageUri = data.getData();

                //pega o stream baseado na URI da imagem
                InputStream inputStream;
                try {
                    inputStream = getContentResolver().openInputStream(imageUri);

                    // pega o bitmap da stream
                    Bitmap image = BitmapFactory.decodeStream(inputStream);

                    MainActivity.images[arrayIndex] = image;
                    finish();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void selectImage(){
        Intent galeryIntent = new Intent(Intent.ACTION_PICK);

        File pictureDiretory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDiretory.getPath();

        Uri data = Uri.parse(pictureDirectoryPath);

        galeryIntent.setDataAndType(data, "image/*");

        startActivityForResult(galeryIntent, ImageSelect.IMAGE_GALLERY_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // se o request tiver sido cancelado, o array virá vazio
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permissão concedida
                    selectImage();
                } else {
                    //permissão não concedida, então não vai colocar a imagem a imagem
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CAMERA_PERMISSION: {
                if(grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    //permissão não concedida
                    Toast.makeText(this, "You can't use camera without permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //metodos referente a camera 2
    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            cameraDevice.close();
            cameraDevice=null;
        }
    };

    private void takePicture(){
        if(cameraDevice == null)
            return;
        CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try{
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            if(characteristics != null)
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                        .getOutputSizes(ImageFormat.JPEG);

            int width = 640;
            int height = 480;
            if(jpegSizes != null && jpegSizes.length > 0)
            {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }
            final ImageReader reader = ImageReader.newInstance(width,height,ImageFormat.JPEG,1);
            List<Surface> outputSurface = new ArrayList<>(2);
            outputSurface.add(reader.getSurface());
            outputSurface.add(new Surface(tvTexture.getSurfaceTexture()));

            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION,ORIENTATIONS.get(rotation));

            //pega o endereço da pasta "Stefanini", e se não tiver sido criada ainda, cria agora
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                    "Stefanini");
            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            // cria o nome do arquivo
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            file = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");

            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader imageReader) {
                    Image image = null;
                    try{
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                        MainActivity.images[arrayIndex] = BitmapFactory.decodeFile(file.getAbsolutePath());
                        finish();

                    }
                    catch (FileNotFoundException e)
                    {
                        e.printStackTrace();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    finally {
                        {
                            if(image != null)
                                image.close();
                        }
                    }
                }
                private void save(byte[] bytes) throws IOException {
                    OutputStream outputStream = null;
                    try{
                        outputStream = new FileOutputStream(file);
                        outputStream.write(bytes);
                    }finally {
                        if(outputStream != null)
                            outputStream.close();
                    }
                }
            };

            reader.setOnImageAvailableListener(readerListener,mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    createCameraPreview();
                }
            };

            cameraDevice.createCaptureSession(outputSurface, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    try{
                        cameraCaptureSession.capture(captureBuilder.build(),captureListener,mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            },mBackgroundHandler);


        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void createCameraPreview() {
        try{
            SurfaceTexture texture = tvTexture.getSurfaceTexture();
            assert  texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(),imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if(cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(ImageSelect.this, "Changed", Toast.LENGTH_SHORT).show();
                }
            },null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void updatePreview() {
        if(cameraDevice == null)
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE,CaptureRequest.CONTROL_MODE_AUTO);
        try{
            cameraCaptureSessions.setRepeatingRequest(
                    captureRequestBuilder.build(),
                    null,mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try{
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map =
                    characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            //checa a permissão
            if(ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                //nao tem permitido, então vai ter que permitir
                ActivityCompat.requestPermissions(this,new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CAMERA_PERMISSION);

            }else{
                manager.openCamera(cameraId,stateCallback,null);
            }

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if(tvTexture.isAvailable())
            openCamera();
        else
            tvTexture.setSurfaceTextureListener(textureListener);
    }

    @Override
    protected void onPause() {
        stopBackgroundThread();
        super.onPause();
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try{
            mBackgroundThread.join();
            mBackgroundThread= null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }
}
