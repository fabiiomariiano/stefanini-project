package br.mobile.stefanini;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import br.mobile.stefanini.adapters.ListStoresAdapter;
import br.mobile.stefanini.beans.Store;
import br.mobile.stefanini.beans.StoresContainer;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    public static final String URL_STORES = "https://api.myjson.com/bins/hvcbf";
    public ListView lvStores;
    public ListStoresAdapter adapter;
    public static Bitmap[] images; // para guardar as imagens das lojas quando selecionadas

    public Bundle savedInstanceState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.savedInstanceState = savedInstanceState;

        lvStores = (ListView) findViewById(R.id.lvStores);
        new GetStoresTask().execute();

        lvStores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long id) {
                Intent newIntent = new Intent(MainActivity.this, StoreDetail.class);
                Store storeClicked = (Store) adapterView.getItemAtPosition(posicao);
                newIntent.putExtra(StoreDetail.EXTRA_STORE, storeClicked);
                startActivity(newIntent);
            }
        });
    }
    /*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("array_images", images);
    }*/

    class GetStoresTask extends AsyncTask<Void, Void, StoresContainer> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(
                    MainActivity.this,
                    MainActivity.this.getResources().getString(R.string.warning) ,
                    MainActivity.this.getResources().getString(R.string.dialogMessage));
        }

        @Override
        protected StoresContainer doInBackground(Void... voids) {
            OkHttpClient client =  new OkHttpClient();
            Request request =  new Request.Builder().url(MainActivity.URL_STORES).build();
            StoresContainer stores = null;
            try{
                Response response = client.newCall(request).execute();
                String jsonString = response.body().string();

                Gson gson = new Gson();
                stores = gson.fromJson(jsonString, StoresContainer.class);

                if(savedInstanceState == null){
                    images = new Bitmap[stores.lojas.size()];
                }else{
                    images = (Bitmap[]) savedInstanceState.getSerializable("array_images");
                }

                adapter = new ListStoresAdapter( MainActivity.this, stores.lojas);
            }catch (Exception e){
                e.printStackTrace();
            }
            return stores;
        }

        @Override
        protected void onPostExecute(StoresContainer stores) {
            lvStores.setAdapter(adapter);
            dialog.dismiss();
        }
    }
}
