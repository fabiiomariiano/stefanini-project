package br.mobile.stefanini.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fabio on 03/01/2018.
 */

public class Store implements Parcelable {
    private int id;
    private Address endereco;
    private String nome;
    private String telefone;

    public Store(int id, Address endereco, String nome, String telefone) {
        this.setId(id);
        this.setEndereco(endereco);
        this.setNome(nome);
        this.setTelefone(telefone);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id > 0){
            this.id = id;
        }else{
            throw new IllegalArgumentException("Invalid id");
        }
    }

    public Address getEndereco() {
        return endereco;
    }

    public void setEndereco(Address endereco) {
        if(endereco != null){
            this.endereco = endereco;
        }else{
            throw new IllegalArgumentException("Invalid address.");
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if(nome != null){
            this.nome = nome;
        }else{
            throw new IllegalArgumentException("Invalid name.");
        }
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        if(telefone != null){
            this.telefone = telefone;
        }else{
            throw new IllegalArgumentException("Invalid phone.");
        }
    }

    public boolean equals(Store another){
        boolean eq = false;
        if(another != null && this.id == another.getId())
            eq = true;
        return eq;
    }

    //metodos referente a interface Parcelable

    private Store(Parcel p){
        id = p.readInt();
        endereco = p.readParcelable(Address.class.getClassLoader());
        nome = p.readString();
        telefone = p.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeParcelable(endereco,0);
        parcel.writeString(nome);
        parcel.writeString(telefone);
    }

    public static final Parcelable.Creator<Store>
            CREATOR = new Parcelable.Creator<Store>() {

        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}
