package br.mobile.stefanini.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fabio on 03/01/2018.
 */

public class Address implements Parcelable {
    private String complemento;
    private String bairro;
    private String numero;
    private String logradouro;

    public Address(String complemento, String bairro, String numero, String logradouro) {
        this.complemento = complemento;
        this.bairro = bairro;
        this.numero = numero;
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        if(logradouro != null) {
            this.logradouro = logradouro;
        }else{
            throw new IllegalArgumentException("Invalid street address.");
        }
    }

    public String toString(){
        String address = "";
        address  = this.logradouro;
        address += ", ";
        address += !this.numero.equals("") ? this.numero : "S/N";
        address += " - ";
        address += this.bairro;
        if(!this.complemento.equals(""))
        address += ", " + this.complemento;
        //se não houver complemento, terminará no bairro
        return address;
    }

    //abaixo metodos referente a interface Parcelable

    private Address(Parcel p){
        complemento = p.readString();
        bairro = p.readString();
        numero = p.readString();
        logradouro = p.readString();
    }

    public static final Parcelable.Creator<Address>
            CREATOR = new Parcelable.Creator<Address>() {

        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(complemento);
        parcel.writeString(bairro);
        parcel.writeString(numero);
        parcel.writeString(logradouro);
    }
}
