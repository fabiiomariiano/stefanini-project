package br.mobile.stefanini.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.mobile.stefanini.R;
import br.mobile.stefanini.beans.Store;

/**
 * Created by Fabio on 03/01/2018.
 */

public class ListStoresAdapter extends BaseAdapter  {

    private LayoutInflater mInflater;
    private List<Store> stores;

    public ListStoresAdapter(Context context, List<Store> stores) {
        this.stores = stores;
        //objeto para o layout do item.
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return stores.size();
    }

    @Override
    public Object getItem(int position) {
        return stores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if (view == null) {
            //se a view for null, então ela não foi criada ainda
            view = mInflater.inflate(R.layout.activity_list_item_stores, null);

            viewHolder = new ViewHolder(view);

            //define os itens na view
            view.setTag(viewHolder);
        } else {
            //se nao for null, entao já foi criada, e só é preciso pegar os itens
            viewHolder = (ViewHolder) view.getTag();
        }

        Store store = this.stores.get(position);
        viewHolder.id.setText(store.getId()+"");
        viewHolder.name.setText(store.getNome());

        return view;
    }

    /**
     * Classe para os itens do layout.
     */
    private class ViewHolder {

        TextView id;
        TextView name;

        public ViewHolder(View myView){
            this.id = (TextView) myView.findViewById(R.id.tvStoreId);
            this.name = (TextView) myView.findViewById(R.id.tvStoreName);
        }
    }
}
