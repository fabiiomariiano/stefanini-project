package br.mobile.stefanini;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import br.mobile.stefanini.beans.Store;

public class StoreDetail extends AppCompatActivity {

    public static final String EXTRA_STORE = "storeClicked";

    public TextView tvNameDetail;
    public TextView tvIdDetail;
    public TextView tvPhoneDetail;
    public TextView tvAddressDetail;
    public ImageButton ibImageDetail;
    public Store mStoreClicked;
    public int arrayIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail);

        this.tvNameDetail = (TextView) findViewById(R.id.tvNameDetail);
        this.tvIdDetail = (TextView) findViewById(R.id.tvIdDetail);
        this.tvPhoneDetail = (TextView) findViewById(R.id.tvPhoneDetail);
        this.tvAddressDetail = (TextView) findViewById(R.id.tvAddressDetail);
        this.ibImageDetail = (ImageButton) findViewById(R.id.ibImageDetail);


        //recupera a loja clicada
        this.mStoreClicked = getIntent().getExtras().getParcelable(EXTRA_STORE);

        //insere as informações
        this.tvNameDetail.setText(this.mStoreClicked.getNome());
        this.tvIdDetail.setText(this.mStoreClicked.getId()+"");
        this.tvPhoneDetail.setText(this.mStoreClicked.getTelefone());
        this.tvAddressDetail.setText(this.mStoreClicked.getEndereco().toString());
        this.arrayIndex = mStoreClicked.getId() - 1;
        if(MainActivity.images[arrayIndex] != null){
            this.ibImageDetail.setImageBitmap(MainActivity.images[arrayIndex] );
        }

        ibImageDetail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
               Intent newIntent = new Intent(StoreDetail.this,ImageSelect.class);
               newIntent.putExtra(ImageSelect.EXTRA_STORE_ARRAY_INDEX, arrayIndex);
               startActivity(newIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(MainActivity.images[arrayIndex] != null){
            this.ibImageDetail.setImageBitmap(MainActivity.images[arrayIndex] );
        }
    }

}
